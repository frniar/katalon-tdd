import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class ErsintaKeyword {
	
	@Keyword
	def login() {
		WebUI.openBrowser('https://www.saucedemo.com/')
		
		WebUI.setText(findTestObject('Object Repository/Ersinta/input_Swag Labs_user-name'), randomString(8))
	
		WebUI.clearText(findTestObject('Object Repository/Ersinta/input_Swag Labs_user-name'))
		
		WebUI.setText(findTestObject('Object Repository/Ersinta/input_Swag Labs_user-name'), 'standard_user')
		
		WebUI.setText(findTestObject('Object Repository/Ersinta/input_Swag Labs_password'), 'secret_sauce')
		
		WebUI.click(findTestObject('Object Repository/Ersinta/input_Swag Labs_login-button'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/button_Add to cart'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/a_1'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/button_Checkout'))
		
		WebUI.setText(findTestObject('Object Repository/Ersinta/input_Checkout Your Information_firstName'), 'Ersinta')
		
		WebUI.setText(findTestObject('Object Repository/Ersinta/input_Checkout Your Information_lastName'), 'Elfandari')
		
		WebUI.setText(findTestObject('Object Repository/Ersinta/input_Checkout Your Information_postalCode'), '12345')
		
		WebUI.click(findTestObject('Object Repository/Ersinta/input_Cancel_continue'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/button_Finish'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/button_Back Home'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/button_Open Menu'))
		
		WebUI.click(findTestObject('Object Repository/Ersinta/a_Logout'))
		
		WebUI.closeBrowser()
		
	}
	
	@Keyword
	def randomString(def length) {
		def password = new java.util.Random().with { r ->
		  def pool = ('a'..'z') + ('A'..'Z') + (0..9)
		  (1..length).collect { pool[r.nextInt(pool.size())] }.join('')
		}
		return password
	}
	
}
