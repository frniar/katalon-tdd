<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password for all userssecret_sauce</name>
   <tag></tag>
   <elementGuidId>a5456e3f-e861-49b0-ba96-1b525cbbf863</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.login_password</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0dd31bd0-a983-4bdd-8805-cc73980b90f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>login_password</value>
      <webElementGuid>bb5e5f38-c0b7-48b5-9a14-462171d6771d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Password for all users:secret_sauce</value>
      <webElementGuid>188ea5fb-6021-4e34-bd9a-35034e7768df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;login_container&quot;]/div[@class=&quot;login_wrapper&quot;]/div[@class=&quot;login_credentials_wrap&quot;]/div[@class=&quot;login_credentials_wrap-inner&quot;]/div[@class=&quot;login_password&quot;]</value>
      <webElementGuid>2f1e0937-9692-4177-8501-4d6d15354230</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div[2]/div/div[2]</value>
      <webElementGuid>af7a4080-1459-4120-b8f3-266010c7bb3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Accepted usernames are:'])[1]/following::div[1]</value>
      <webElementGuid>6aef9803-d3f3-45ca-b5e9-1d3f8a11b900</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='secret_sauce']/parent::*</value>
      <webElementGuid>715fdbd2-99ee-40b3-8f00-b442f92a4494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]</value>
      <webElementGuid>7e2c97df-9f7d-44a7-b158-58bfeaebb947</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Password for all users:secret_sauce' or . = 'Password for all users:secret_sauce')]</value>
      <webElementGuid>9b527846-703c-4c56-95e3-576a3d2f72d1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
