<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Cancel_continue</name>
   <tag></tag>
   <elementGuidId>9c66a093-64b7-4b33-b999-6e7347e45a74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#continue</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='continue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>5faf1562-1ad7-40e3-852e-61ca6ef467fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>7f9b3c37-c23e-456f-919b-4e67097730a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn btn_primary cart_button btn_action</value>
      <webElementGuid>c0b60c19-b9fe-452f-8a46-116db45ab4ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>8f08083a-628d-4ab2-85b7-349a92597725</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>cd0ab30e-3171-46f5-b80c-aecbf89f2c44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>continue</value>
      <webElementGuid>99cba08a-0c92-409e-9303-77e8c35818f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Continue</value>
      <webElementGuid>8e3341be-1646-43cb-9383-cca9c1b2db31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;continue&quot;)</value>
      <webElementGuid>75ff32f4-36a7-43e7-a344-123328fd02d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='continue']</value>
      <webElementGuid>a256a1a1-6727-48b4-a454-5f580aa74c43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div[2]/input</value>
      <webElementGuid>45fc35a1-6ab0-48d6-b48f-85b87f283583</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>e0e4a717-7c58-4f71-a323-a12aac42c9f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'continue' and @name = 'continue']</value>
      <webElementGuid>cdaf699e-e886-4413-9466-095346791f39</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
