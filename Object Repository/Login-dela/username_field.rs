<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>username_field</name>
   <tag></tag>
   <elementGuidId>c1d03c90-f34e-4bbb-8e54-a0b702e46e75</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'username']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>6dd6fc97-27d9-493b-ba3e-ea54164b2342</webElementGuid>
   </webElementProperties>
</WebElementEntity>
